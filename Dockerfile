FROM fedora:latest

RUN \
    dnf -y install cmake openssh-clients openssh-server && \
    dnf -y clean all && \
    touch /run/utmp && \
    sed -i '/^PermitRootLogin/ d' /etc/ssh/sshd_config && \
    echo "PermitRootLogin yes" >> /etc/ssh/sshd_config && \
    echo "root:root" | chpasswd

COPY entrypoint.sh /

EXPOSE 22
ENTRYPOINT ["/entrypoint.sh"]
